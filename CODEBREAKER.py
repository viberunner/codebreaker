#!/bin/python3
# by viberunner

# you know these "ciphers" where actually people just replace letters for other symbols?
# yeah, this is the thing supposed to break these
# this iteration will be highly inefficient;
#	there will be a lot of redundant checks because of repeating characters
#	also probably multithreading would be very handy


def all_unique(lst):
	for i in range(len(lst)):
		if lst.count(lst[i]) > 1:
			return False
	return True


from tqdm import tqdm

# === STAGE 1. PREPARATION

# creating the charlist and choosing the wordlist
lang = input("\n[P]olish or [E]nglish?\n> ")

if lang.lower() in ["p", "pl", "pol", "polish"]:
	wordlist = "polish.txt"
	charlist = "AĄBCĆDEĘFGHIJKLŁMNŃOÓPRSŚTUWYZŹŻ"

elif lang.lower() in ["e", "en", "eng", "english"]:
	wordlist = "english.txt"
	charlist = "ABCDEFGHIJKLMNOPRSTUWYZQXV"

else:
	exit("\nLanguage not found.\n")

#adding special characters on will
charlist += ".!?," #';:()       you can do more, but i only do common speaking language ones


# converting the specialtext into something we can work with
ciphertext = input("\nInput the ciphertext, replacing identical symbols to rising letters:\n>").upper() #like this: ABCD CED, also strip it from any special characters
print()

temp = []
key = []
for char in ciphertext:
	if char != ' ' and char not in temp:
		temp.append(char)
		key.append(charlist[0])

del temp

matrix = []
guide = "ABCDEFGHIJKLMNOPRSTUWYZ" #!!!!!!!!!!!!!!! WRITE THE CIPHERTEXT ACCORDINGLY WITH THIS THING, LIKE IN ORDER CONVERT CHARACTERS
for a in list(ciphertext):
	if a == ' ':
		matrix.append(' ')
	else:
		matrix.append(guide.index(a))

del guide


# === STAGE 2. EXECUTION
for i in tqdm(range(len(charlist)**len(key))):

	if all_unique(key) == True:

		temp = ''
		for char in matrix:
			if type(char) == int:
				temp += key[char]
			else:
				temp += " "


		# finding words 
		legit = []
		for word in temp.split(' '):
			with open(wordlist) as file:
				if word.lower() in file.read():
					legit.append(word)
		if len(legit) >= len(temp.split(' '))/2:
			print(temp)


	# assigning symbols one up
	tryindex = 0
	while True:

		try:
			key[tryindex] = charlist[charlist.index(key[tryindex])+1]
			break
		except Exception:
			key[tryindex] = charlist[0]
			tryindex += 1

		if tryindex == len(key):
			break
			print("=== ALL DONE ===")

print("\n\n")
for a in legit:
	print(a)